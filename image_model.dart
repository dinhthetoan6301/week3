import 'package:http/http.dart' as http;
import 'dart:convert';

String jsonRaw = '{ "id": 1, "url": "https://via.placeholder.com/600/92c952"}';

class ImageModel {
  int? id;
  String? url;

  ImageModel(this.id, this.url);

  ImageModel.fromJson(jsonObject) {
    id = jsonObject['id'];
    url = jsonObject['url'];
  }

  @override
  String toString() {
    return '($id, $url)';
  }
}

void main() {
  var jsonObject = json.decode(jsonRaw);

  var image1 = ImageModel(jsonObject['id'], jsonObject['url']);
  var image2 = ImageModel.fromJson(jsonObject);
  print(image1);
  print(image2);
}
